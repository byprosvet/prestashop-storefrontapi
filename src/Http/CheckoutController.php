<?php

namespace Byprosvet\StorefrontApi\Http;

use Byprosvet\StorefrontApi\Checkout;
use Symfony\Component\HttpFoundation\Request;
use Byprosvet\StorefrontApi\Catalog\CarrierTable;
use Byprosvet\StorefrontApi\Catalog\CountryTable;
use Byprosvet\StorefrontApi\Checkout\StartRequest;
use Symfony\Component\HttpFoundation\JsonResponse;

class CheckoutController
{
    const PROMO_CATEGORY_ID = 10;
    const MAIN_CATEGORY_ID = 11;
    const SAMVYDAT_CATEGORY_ID = 12;

    public function __construct(
        private CountryTable $countryTable,
        private CarrierTable $carrierTable,
        private Checkout $checkout
    ) {}

    public function getCountriesAction()
    {
        return new JsonResponse([
            'countries' => $this->countryTable->getAll()
        ]);
    }

    public function findDeliveryOptionsAction(Request $request)
    {
        $countryCode = $request->query->get('country_code');
        $deliveryOptions = [];
        if (isset($countryCode)) {
            $deliveryOptions = $this->carrierTable->findCarriersForCountry($countryCode);
        }
        return new JsonResponse([
            'deliveryOptions' => $deliveryOptions
        ]);
    }

    public function startAction(Request $request)
    {
        $payload = json_decode(
            $request->getContent(),
            true,
            512,
            \JSON_BIGINT_AS_STRING | \JSON_THROW_ON_ERROR
        );
        $startRequest = StartRequest::fromArray($payload);

        return new JsonResponse($this->checkout->start($startRequest));
    }
}
