<?php

namespace Byprosvet\StorefrontApi\Http;

use Byprosvet\StorefrontApi\Catalog\ProductTable;
use Byprosvet\StorefrontApi\Catalog\CategoryTable;
use Byprosvet\StorefrontApi\Catalog\LanguageTable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CatalogController
{
    const PROMO_CATEGORY_ID = 10;
    const MAIN_CATEGORY_ID = 11;
    const SAMVYDAT_CATEGORY_ID = 12;

    public function __construct(
        private CategoryTable $categoryTable,
        private ProductTable $productTable,
        private LanguageTable $languageTable,
    ) {}

    public function getAction()
    {
        return new JsonResponse([
            'promo' => $this->productTable->findProductsByCategoryId([self::PROMO_CATEGORY_ID]),
            'samvydat' => $this->categoryTable->getCategoryWithProductsById(self::SAMVYDAT_CATEGORY_ID, 2),
            'collections' => $this->categoryTable->getCategoriesWithProductsByParentId(self::MAIN_CATEGORY_ID),
            'languages' => $this->languageTable->findAllLanguages()
        ]);
    }

    public function getProductAction(Request $request, $match)
    {
        $productId = $match['productId'];
        $product = $this->productTable->findProductById($productId);

        return new JsonResponse([
            'product' => $product,
            'recommendations' => $this->productTable->findRecommendations($productId),
        ]);
    }
}
