<?php

namespace Byprosvet\StorefrontApi;

use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Byprosvet\StorefrontApi\Http\CatalogController;
use Byprosvet\StorefrontApi\Http\CheckoutController;

class Router
{
    private RouteCollection $routes;
    public function __construct(
        CatalogController $catalogController,
        CheckoutController $checkoutController
    ) {
        $this->routes = new RouteCollection();
        $this->routes->add(
            'index',
            new Route(
                '/',
                [
                    '_controller' => $catalogController,
                    'action' => 'getAction',
                    'methods' => ['GET'],
                ]
            )
        );
        $this->routes->add(
            'product.id',
            new Route(
                '/product/{productId}',
                [
                    '_controller' => $catalogController,
                    'action' => 'getProductAction',
                    'methods' => ['GET'],
                ]
            )
        );
        $this->routes->add(
            'checkout.countries',
            new Route(
                '/checkout/countries',
                [
                    '_controller' => $checkoutController,
                    'action' => 'getCountriesAction',
                    'methods' => ['GET'],
                ]
            )
        );
        $this->routes->add(
            'checkout.deliveryOptions',
            new Route(
                '/checkout/delivery-options',
                [
                    '_controller' => $checkoutController,
                    'action' => 'findDeliveryOptionsAction',
                    'methods' => ['GET'],
                ]
            )
        );
        $this->routes->add(
            'checkout.add',
            new Route(
                '/checkout/',
                [
                    '_controller' => $checkoutController,
                    'action' => 'startAction',
                    'methods' => ['POST'],
                ]
            )
        );
        $this->routes->addPrefix('/storefrontapi');
    }

    public function match(Request $request)
    {
        $context = new RequestContext();
        $context->fromRequest($request);
        $matcher = new UrlMatcher($this->routes, $context);
        try {
            $match = $matcher->match($request->getPathInfo());
            if (!$match) return null;
        } catch (\Throwable $error) {
            return null;
        }

        $hasMethod = in_array($request->getMethod(), $match['methods']);
        if (!$hasMethod) return null;
        return $match;
    }
}
