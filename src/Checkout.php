<?php

namespace Byprosvet\StorefrontApi;

use Byprosvet\StorefrontApi\Catalog\CountryTable;
use Byprosvet\StorefrontApi\Catalog\CurrencyTable;
use Byprosvet\StorefrontApi\Checkout\StartRequest;
use Byprosvet\StorefrontApi\Stripe\Payments;

class Checkout
{
    public function __construct(
        private Payments $stripePayments,
        private CountryTable $countryTable,
        private CurrencyTable $currencyTable
    ) {
    }

    public function start(StartRequest $startRequest)
    {
        $id_currency = $this->currencyTable->getDefaultCurrency();
        $id_lang = 2;
        $id_shop = 1;

        $id_country = $this->countryTable->getIdByIsoCode(
            $startRequest->carrier->countryCode
        );
        $currency = new \Currency($id_currency);
        $customer = new \Customer();
        $customer->lastname = $startRequest->contact->lastname;
        $customer->firstname = $startRequest->contact->firstname;
        $customer->email = $startRequest->contact->email;
        $customer->is_guest = true;
        $customer->setWsPasswd(bin2hex(openssl_random_pseudo_bytes(4)));
        $customer->save();
        $address = new \CustomerAddress();
        $address->alias = 'auto-storefrontapi';
        $address->id_customer = $customer->id;
        $address->id_country = $id_country;
        $address->firstname = $customer->firstname;
        $address->lastname = $customer->lastname;
        $address->city = $startRequest->carrier->city;
        $address->address1 = $startRequest->carrier->address1;
        $address->address2 = $startRequest->carrier->address2;
        $address->save();
        $cart = new \Cart();
        $cart->id_currency = $id_currency;
        $cart->id_lang = $id_lang;
        $cart->id_customer = $customer->id;
        $cart->id_shop = $id_shop;
        $cart->id_address_delivery = $address->id;
        $cart->id_address_invoice = $address->id;
        $cart->id_carrier = $startRequest->carrier->id;
        $cart->save();
        $cart->setWsCartRows(
            array_map(
                fn ($cartItem) => [
                    "id_product" => $cartItem->id,
                    "quantity" => $cartItem->quantity
                ],
                $startRequest->cart
            )
        );
        $cart->save();

        $context = \Context::getContext();
        $context->cart = $cart;
        $context->customer = $customer;
        $context->currency = $currency;

        $checkoutData = $this->stripePayments->createPaymentIntentFromCart($context);

        $stripeUrl = $checkoutData['url'];

        return [
            'stripeUrl' => $stripeUrl
        ];
    }
}
