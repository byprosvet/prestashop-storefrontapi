<?php

namespace Byprosvet\StorefrontApi\Catalog;

use Doctrine\DBAL\Connection;

class CountryTable
{
    public function __construct(
        private Connection $connection,
        private LanguageTable $languageTable
    ) {
    }

    public function getAll()
    {
        $raw_countries = $this->connection->fetchAllAssociative("
            SELECT c.iso_code, cl.*
            FROM ps_country c
            INNER JOIN ps_country_lang cl on cl.id_country = c.id_country
            ORDER BY c.iso_code, cl.id_lang
        ");
        $countries = [];
        foreach ($raw_countries as $raw_country)
        {
            $countries[$raw_country['id_country']] = $countries[$raw_country['id_country']] ?? [
                'id' => $raw_country['id_country'],
                'iso_code' => $raw_country['iso_code'],
                'name' => []
            ];
            $countries[$raw_country['id_country']]['name'][$raw_country['id_lang']] = $this->languageTable->addLangCodes([
                'id' => $raw_country['id_lang'],
                'value' => $raw_country['name']
            ]);
        }
        foreach ($countries as $id => $country)
        {
            $country['name'] = array_values($country['name']);
            $countries[$id] = $country;
        }
        return array_values($countries);
    }

    public function getIdByIsoCode(string $isoCode)
    {
        $columns = $this->connection->fetchFirstColumn("
            SELECT c.id_country
            FROM ps_country c
            WHERE c.iso_code = :iso_code
        ", [ "iso_code" => $isoCode ]);

        return $columns[0];
    }
}
