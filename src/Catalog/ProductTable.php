<?php

namespace Byprosvet\StorefrontApi\Catalog;

use Doctrine\DBAL\Connection;

class ProductTable
{
    public function __construct(
        private Connection $connection,
        private LanguageTable $languageTable,
        private ImageTable $imageTable
    ) {
    }

    public function findProductsByCategoryId(array $categoryIds)
    {
        $raw_products = $this->connection->fetchAllAssociative("
            SELECT
            p.id_product, p.price, pl.id_lang, pl.name, pl.description, pl.description_short, pl.link_rewrite, cp.id_category, cp.position
            FROM ps_product p
            inner join ps_product_lang pl on pl.id_product = p.id_product
            inner join ps_category_product cp on cp.id_product = p.id_product
            WHERE p.active = 1 and cp.id_category IN (:id)
            ORDER BY cp.position ASC
            ",
            ['id' => $categoryIds],
            ['id' => Connection::PARAM_INT_ARRAY]
        );
        return $this->mapToProduct($raw_products);
    }

    public function findRecommendations($productId)
    {
        $categoryIds = $this->connection->fetchFirstColumn("
            SELECT cp.id_category FROM ps_category_product cp
            WHERE cp.id_product = :id
            ",
            ['id' => $productId]
        );
        return $this->findProductsByCategoryId($categoryIds);
    }

    public function findProductById($productId)
    {
        $raw_products = $this->connection->fetchAllAssociative("
            SELECT
            p.id_product, p.price, pl.id_lang, pl.name, pl.description, pl.description_short, pl.link_rewrite, cp.id_category, cp.position
            FROM ps_product p
            inner join ps_product_lang pl on pl.id_product = p.id_product
            inner join ps_category_product cp on cp.id_product = p.id_product
            WHERE p.active = 1 and p.id_product = :id
            ORDER BY cp.position ASC
            ",
            ['id' => $productId]
        );
        $products = $this->mapToProduct($raw_products);
        if (count($products) > 1 || count($products) < 1)
        {
            return null;
        }
        $product = $products[0];
        $raw_images = $this->imageTable->findForProducts([$productId]);
        $images = [];
        foreach ($raw_images as $raw_image)
        {
            $images[] = $this->getImage($product, $raw_image['id_image']);
        }
        $product['images'] = $images;
        return $product;
    }

    private function filterCover($covers, $productId)
    {
        foreach ($covers as $cover)
        {
            if ($cover['id_product'] === $productId)
            {
                return $cover['id_image'];
            }
        }
    }

    private function mapToProduct(array $raw_products)
    {
        $products = [];
        foreach ($raw_products as $raw_product)
        {
            $product = $products[$raw_product['id_product']] ?? [
                'id' => $raw_product['id_product'],
                'price' => $raw_product['price'],
                'name' => [],
                'description' => [],
                'description_short' => [],
                'link_rewrite' => [],
                'id_category' => [],
                'images' => [],
            ];
            $product['id_category'] = array_unique(
                array_merge($product['id_category'], [$raw_product['id_category']])
            );
            if (!empty($raw_product['name']))
            {
                $product['name'][$raw_product['id_lang']] = $this->languageTable->addLangCodes(['id' => $raw_product['id_lang'], 'value' => $raw_product['name']]);
            }
            if (!empty($raw_product['description']))
            {
                $product['description'][$raw_product['id_lang']] = $this->languageTable->addLangCodes(['id' => $raw_product['id_lang'], 'value' => $raw_product['description']]);
            }
            if (!empty($raw_product['description_short']))
            {
                $product['description_short'][$raw_product['id_lang']] = $this->languageTable->addLangCodes(['id' => $raw_product['id_lang'], 'value' => $raw_product['description_short']]);
            }
            if (!empty($raw_product['link_rewrite']))
            {
                $product['link_rewrite'][$raw_product['id_lang']] = $this->languageTable->addLangCodes(['id' => $raw_product['id_lang'], 'value' => $raw_product['link_rewrite']]);
            }
            $products[$product['id']] = $product;
        }
        $covers = $this->imageTable->findCovers(
            array_map(fn ($raw_product) => $raw_product['id_product'], $raw_products),
        );
        foreach($products as $id => $product)
        {
            $coverId = $this->filterCover($covers, $id);
            $product['featuredImage'] = $this->getImage($product, $coverId);
            $product['images'][] = $product['featuredImage'];
            $product['name'] = array_values($product['name']);
            $product['description'] = array_values($product['description']);
            $product['description_short'] = array_values($product['description_short']);
            $product['link_rewrite'] = array_values($product['link_rewrite']);
            $products[$id] = $product;
        }
        return array_values($products);
    }

    private function getImage($product, $imageId)
    {
        $link = new \Link(null, 'https://');
        $link_rewrite = null;
        foreach ($product['link_rewrite'] as $rewrite)
        {
            if (isset($rewrite['value'])) {
                $link_rewrite = $rewrite['value'];
                break;
            }
        }
        if (!$link_rewrite) {
            return null;
        }
        return [
            'id' => $imageId,
            'url' => $link->getImageLink(
                $link_rewrite,
                $imageId,
                'large_default'
            ),
            'width' => 800,
            'height' => 800
        ];
    }
}
