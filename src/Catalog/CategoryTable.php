<?php

namespace Byprosvet\StorefrontApi\Catalog;

use Doctrine\DBAL\Connection;

class CategoryTable
{
    public function __construct(
        private Connection $connection,
        private ProductTable $productTable
    ) {
    }

    public function getCategoriesWithProductsByParentId(string $parentId)
    {
        $raw_collections = $this->connection->fetchAllAssociative("SELECT c.id_category, cl.id_lang, cl.name, cl.description, cl.link_rewrite
            FROM ps_category c
            inner join ps_category_lang cl on cl.id_category = c.id_category
            where c.active = 1 and c.id_parent = :id
            ORDER BY c.position ASC", ['id' => $parentId]
        );
        $collections = [];
        foreach ($raw_collections as $raw_collection) {
            $collection = $collections[$raw_collection['id_category']] ?? [
                'id' => $raw_collection['id_category'],
                'name' => [],
                'description' => [],
                'link_rewrite' => [],
            ];
            $collection['name'][] = ['id' => $raw_collection['id_lang'], 'value' => $raw_collection['name']];
            $collection['description'][] = ['id' => $raw_collection['id_lang'], 'value' => $raw_collection['description']];
            $collection['link_rewrite'][] = ['id' => $raw_collection['id_lang'], 'value' => $raw_collection['link_rewrite']];
            $collections[$collection['id']] = $collection;
        }
        $collections = array_values($collections);
        $collectionsId = array_map(fn ($collection) => $collection['id'], $collections);
        $products = $this->productTable->findProductsByCategoryId($collectionsId);
        $collections = array_map(fn ($collection) => $this->addProducts($collection, $products), $collections);
        return $collections;
    }

    public function getCategoryWithProductsById(string $id, $limit = 10)
    {
        $raw_collections = $this->connection->fetchAllAssociative("SELECT c.id_category, cl.id_lang, cl.name, cl.description, cl.link_rewrite
            FROM ps_category c
            inner join ps_category_lang cl on cl.id_category = c.id_category
            where c.active = 1 and c.id_category = :id
            ORDER BY c.position ASC", ['id' => $id]
        );
        $collections = [];
        foreach ($raw_collections as $raw_collection) {
            $collection = $collections[$raw_collection['id_category']] ?? [
                'id' => $raw_collection['id_category'],
                'name' => [],
                'description' => [],
                'link_rewrite' => [],
            ];
            $collection['name'][] = ['id' => $raw_collection['id_lang'], 'value' => $raw_collection['name']];
            $collection['description'][] = ['id' => $raw_collection['id_lang'], 'value' => $raw_collection['description']];
            $collection['link_rewrite'][] = ['id' => $raw_collection['id_lang'], 'value' => $raw_collection['link_rewrite']];
            $collections[$collection['id']] = $collection;
        }
        $collection = $collections[$id];
        $collection['products'] = array_slice($this->productTable->findProductsByCategoryId([$id]), -$limit);
        return $collection;
    }

    private function addProducts($collection, $products)
    {
        $collection['products'] = array_values(array_filter(
            $products,
            fn ($product) => in_array($collection['id'], $product['id_category'])
        ));
        return $collection;
    }
}
