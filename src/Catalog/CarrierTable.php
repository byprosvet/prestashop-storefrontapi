<?php

namespace Byprosvet\StorefrontApi\Catalog;

use Doctrine\DBAL\Connection;

class CarrierTable
{
    public function __construct(
        private Connection $connection,
        private CurrencyTable $currencyTable
    ) {
    }

    public function findCarriersForCountry(string $countryCode)
    {
        $carriers = $this->connection->fetchAllAssociative(
            "SELECT cz.id_carrier, cr.name, cr.is_free
            FROM ps_carrier_zone cz
            INNER JOIN ps_country c on c.id_zone = cz.id_zone
            INNER JOIN ps_carrier cr on cr.id_carrier = cz.id_carrier
            WHERE c.iso_code = :iso_code AND cr.active = 1 AND cr.deleted = 0
            ORDER BY cr.position
            ",
            ['iso_code' => $countryCode]
        );
        $carriers = array_map(fn ($carrier) => $this->prepareDeliveryOption($carrier), $carriers);

        $nonFreeCarriers = array_filter($carriers, fn ($carrier) => !$carrier['isFree']);

        $deliveryPrices = $this->connection->fetchAllAssociative(
            "SELECT c.id_carrier, d.price
            FROM ps_carrier c
            INNER JOIN ps_delivery d on d.id_carrier = c.id_carrier
            INNER JOIN ps_country country on country.id_zone = d.id_zone AND country.iso_code = :iso_code
            WHERE c.id_carrier IN (:id_carrier) AND c.active = 1 AND c.deleted = 0
            ",
            [
                'id_carrier' => array_map(fn ($carrier) => $carrier['id'], $nonFreeCarriers),
                'iso_code' => $countryCode
            ],
            ['id_carrier' => Connection::PARAM_INT_ARRAY]
        );

        return array_map(fn ($carrier) => $this->preparePrice($carrier, array_reduce(
                $deliveryPrices,
                fn($currentPrice, $deliveryPrice) => $deliveryPrice['id_carrier'] === $carrier['id']
                    ? $deliveryPrice['price']
                    : $currentPrice,
                null
            )
        ), $carriers);
    }

    public function findById($id)
    {
        $carriers = $this->connection->fetchAllAssociative(
            "SELECT cz.id_carrier, cr.name, cr.is_free
            FROM ps_carrier_zone cz
            INNER JOIN ps_country c on c.id_zone = cz.id_zone
            INNER JOIN ps_carrier cr on cr.id_carrier = cz.id_carrier
            WHERE cr.id_carrier = :id_carrier
            ORDER BY cr.position
            ",
            ['id_carrier' => $id]
        );
        $carrier = $this->prepareDeliveryOption($carriers[0]);

        $deliveryPrices = $this->connection->fetchAllAssociative(
            "SELECT c.id_carrier, d.price
            FROM ps_carrier c
            INNER JOIN ps_delivery d on d.id_carrier = c.id_carrier
            WHERE c.id_carrier IN (:id_carrier) AND c.active = 1 AND c.deleted = 0
            ",
            [
                'id_carrier' => $carrier['id'],
            ],
            ['id_carrier' => Connection::PARAM_INT_ARRAY]
        );

        return array_map(
            fn ($carrier) => $this->preparePrice(
                $carrier,
                array_reduce(
                    $deliveryPrices,
                    fn($currentPrice, $deliveryPrice) => $deliveryPrice['id_carrier'] === $carrier['id']
                        ? $deliveryPrice['price']
                        : $currentPrice,
                    null
                )
            ),
            $carriers
        );
    }

    private function prepareDeliveryOption($carrier)
    {
        return [
            'id' => $carrier['id_carrier'],
            'id_carrier' => $carrier['id_carrier'],
            'name' => $carrier['name'],
            'isFree' => $carrier['is_free'] === 1,
            'requiredInformation' => $this->prepareRequiredInformation($carrier['id_carrier']),
            'price' => null
        ];
    }

    private function preparePrice($deliveryOption, $price)
    {
        return array_merge($deliveryOption, [
            'price' => $this->currencyTable->formatPriceForDefaultCurrency($price)
        ]);
    }

    private function prepareRequiredInformation($deliveryOptionId)
    {
        switch ($deliveryOptionId) {
            case 9:
                return "code";
            case 6:
            case 15:
                return "address";
        }
    }
}
