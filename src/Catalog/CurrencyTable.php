<?php

namespace Byprosvet\StorefrontApi\Catalog;

use Doctrine\DBAL\Connection;
use PrestaShop\PrestaShop\Core\Localization\Number\Formatter as NumberFormatter;
use PrestaShop\Decimal\Operation\Rounding;
use PrestaShop\PrestaShop\Core\Localization\Specification\Price as PriceSpecification;
use PrestaShop\PrestaShop\Core\Localization\Specification\NumberSymbolList;

class CurrencyTable
{
    public function __construct(
        private Connection $connection
    ) {
    }

    public function getDefaultCurrency()
    {
        return 2;
    }

    public function formatPriceForDefaultCurrency($price)
    {
        $formatter = new NumberFormatter(
            Rounding::ROUND_HALF_UP,
            'latn'
        );
        $spec = new PriceSpecification(
            '#,##0.##',
            '-#,##0.##',
            ['latn' => new NumberSymbolList(',', ' ', ';', '%', '-', '+', 'E', '^', '‰', '∞', 'NaN')],
            2,
            2,
            true,
            3,
            3,
            'symbol',
            'zł',
            'PLN'
        );

        return $formatter->format(
            (float) $price,
            $spec
        );
    }
}
