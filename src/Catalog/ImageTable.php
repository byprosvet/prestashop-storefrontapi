<?php

namespace Byprosvet\StorefrontApi\Catalog;

use Doctrine\DBAL\Connection;

class ImageTable
{
    public function __construct(
        private Connection $connection
    ) {
    }

    public function findCovers(array $productsId)
    {
        $raw_images = $this->connection->fetchAllAssociative("SELECT i.id_image, i.id_product
            FROM ps_image i
            WHERE i.cover = 1 AND i.id_product IN (:id)
            ",
            ['id' => $productsId],
            ['id' => Connection::PARAM_INT_ARRAY]
        );
        return $raw_images;
    }

    public function findForProducts(array $productsId)
    {
        $raw_images = $this->connection->fetchAllAssociative("SELECT i.id_image, i.id_product, position, i.cover
            FROM ps_image i
            WHERE i.id_product IN (:id)
            ORDER BY i.position ASC",
            ['id' => $productsId],
            ['id' => Connection::PARAM_INT_ARRAY]
        );
        return $raw_images;
    }
}
