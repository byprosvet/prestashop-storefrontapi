<?php

namespace Byprosvet\StorefrontApi\Catalog;

use Doctrine\DBAL\Connection;

class LanguageTable
{
    public function __construct(
        private Connection $connection,
        private $languages = null
    )
    {
    }

    public function findAllLanguages()
    {
        if (!$this->languages)
        {
            $this->languages = $this->connection->fetchAllAssociative("Select * FROM ps_lang WHERE active = 1");
        }
        return $this->languages;
    }

    public function addLangCodes(array $translatedValue)
    {
        $languages = $this->findAllLanguages();
        $langForValue = null;
        foreach ($languages as $language) {
            if ($language['id_lang'] === $translatedValue['id']) {
                $langForValue = $language;
                break;
            }
        }
        return array_merge($translatedValue, [
            'iso_code' => $langForValue['iso_code']
        ]);
    }
}
