<?php

namespace Byprosvet\StorefrontApi\Stripe;

use Stripe\PaymentIntent;
use Stripe\Checkout\Session;
use Stripe\Exception\ApiErrorException;

class Payments
{
    public function createPaymentIntentFromCart(\Context $context)
    {
        $checkoutData = [];

        try {
            $cart = $context->cart;

            $language = new \Language($cart->id_lang);
            $currency = new \Currency($cart->id_currency);
            $shippingAddress = new \Address($context->cart->id_address_delivery);
            $shippingAddressState = new \State($shippingAddress->id_state);
            $country = \Country::getIsoById($shippingAddress->id_country);
            $shippingAddress =  $this->getShippingDetails($shippingAddress, $shippingAddressState, $country, $context->customer);

            $intentData = $this->constructIntentData($context->cart->id, true, \Configuration::get(Configuration::CATCHANDAUTHORIZE));
            $intentData['shipping'] = $shippingAddress;
            $finalPrice = $cart->getOrderTotal() * 100;

            $lineItems[] = [
                'price_data' => [
                    'currency' => $currency->iso_code,
                    'unit_amount_decimal' => round($finalPrice, 2),
                    'product_data' => [
                        'name' => 'Products Purchase',
                    ],
                ],
                'quantity' => 1,
            ];

            $customer =  $this->getCustomerDetails($context->customer, $intentData);
            $checkoutData = $this->constructCheckoutData($context, $intentData, $lineItems, $customer, $language);

            try {
                $this->createIdempotencyKey($context->cart, $intentData, $checkoutData->id);
            } catch (\Stripe\Exception\ApiErrorException $e) {
                $this->exceptionErrorLogger('Create Stripe Intent Error => ' . $e->getMessage(), 'Checkout - createIdempotencyKey');
            } catch (\PrestaShopException $e) {
                $this->exceptionErrorLogger('Save Stripe Idempotency Key Error => ' . $e->getMessage(), 'Checkout - createIdempotencyKey');
            }
        } catch (ApiErrorException $e) {
            return $this->exceptionErrorLogger('Retrieve Stripe Account Error => ' . $e->getMessage(), 'initContent - constructCheckoutData');
        } catch (\PrestaShopDatabaseException $e) {
            return $this->exceptionErrorLogger('Retrieve Prestashop State Error => ' . $e->getMessage(), 'initContent - constructCheckoutData');
        } catch (\PrestaShopException $e) {
            return $this->exceptionErrorLogger('Retrieve Prestashop State Error => ' . $e->getMessage(), 'initContent - constructCheckoutData');
        }

        return $checkoutData;
    }

    private function constructCheckoutData($context, $intent, $lineItems, $customer = null, $locale = null)
    {
        $checkoutSession = [];
        try {
            $stripeOrderFailureReturnUrl = $context->link->getModuleLink(
                'stripe_official',
                'orderFailure',
                [],
                true
            );

            $stripeOrderSuccessReturnUrl = $context->link->getModuleLink(
                'stripe_official',
                'orderConfirmationReturn',
                ['cartId' => $context->cart->id],
                true
            );

            $checkoutParams = [
                'line_items' => $lineItems,
                'payment_intent_data' => $intent,
                'mode' => Session::MODE_PAYMENT,
                'locale' => 'auto',
                'metadata' => [
                    'id_cart' => $context->cart->id,
                ],
                'success_url' => $stripeOrderSuccessReturnUrl,
                'cancel_url' => $stripeOrderFailureReturnUrl,
            ];
            if ($customer) {
                $checkoutParams['customer'] = $customer;
            }
            $checkoutSession = Session::create($checkoutParams);

        } catch (ApiErrorException $e) {
            return $this->exceptionErrorLogger('Retrieve Stripe Account Error => ' . $e->getMessage(), 'constructCheckoutData');
        } catch (\PrestaShopDatabaseException $e) {
            return $this->exceptionErrorLogger('Retrieve Prestashop State Error => ' . $e->getMessage(), 'constructCheckoutData');
        } catch (\PrestaShopException $e) {
            return $this->exceptionErrorLogger('Retrieve Prestashop State Error => ' . $e->getMessage(), 'constructCheckoutData');
        }

        return $checkoutSession;
    }

    private function getShippingDetails($shippingAddress, $shippingAddressState, $country, $customer)
    {
        $customerFullName = $this->getCustomerFullNameContext($customer);

        return [
            'name' => $customerFullName,
            'address' => [
                'line1' => $shippingAddress->address1,
                'line2' => $shippingAddress->address2,
                'postal_code' => $shippingAddress->postcode,
                'city' => $shippingAddress->city,
                'state' => $shippingAddressState->iso_code,
                'country' => $country,
            ],
        ];
    }

    private function getPaymentMethodOptions($captureMethod)
    {
        return [
            'link' => [
                'capture_method' => $captureMethod,
            ],
            'card' => [
                'capture_method' => $captureMethod,
            ],
            'klarna' => [
                'capture_method' => $captureMethod,
            ],
            'afterpay_clearpay' => [
                'capture_method' => $captureMethod,
            ],
            'affirm' => [
                'capture_method' => $captureMethod,
            ],
            'wechat_pay' => [
                'client' => 'web',
            ],
        ];
    }

    private function createBillingDetails($addressDetails, $shippingAddressState, $customer, $country)
    {
        return [
            'billing_details' => [
                'address' => [
                    'city' => $addressDetails->city,
                    'country' => $country,
                    'line1' => $addressDetails->address1,
                    'line2' => $addressDetails->address2,
                    'postal_code' => $addressDetails->postcode,
                    'state' => $shippingAddressState->iso_code,
                ],
                'email' => $customer->email,
                'name' => $this->getCustomerFullNameContext($customer),
                'phone' => $addressDetails->phone,
            ],
        ];
    }

    private function getCustomerDetails($customer, $intent)
    {
        $stripeAccount = \Stripe\Account::retrieve();
        $stripeCustomer = new \StripeCustomer();
        $stripeCustomer = $stripeCustomer->getCustomerById($customer->id, $stripeAccount->id);

        if ((true === empty($stripeCustomer->id_customer) || null == $stripeCustomer->id_customer) && 0 == $customer->is_guest) {
            $customerData = \Stripe\Customer::create([
                'description' => 'Customer created from Prestashop Stripe module',
                'email' => $customer->email,
                'name' => $customer->firstname . ' ' . $customer->lastname,
                'address' => [
                    'city' => $intent['shipping']['address']['city'],
                    'country' => $intent['shipping']['address']['country'],
                    'line1' => $intent['shipping']['address']['line1'],
                    'postal_code' => $intent['shipping']['address']['postal_code'],
                    'state' => $intent['shipping']['address']['state'],
                ],
            ]);

            $stripeCustomer->id_customer = $customer->id;
            $stripeCustomer->stripe_customer_key = $customerData->id;
            $stripeCustomer->id_account = $stripeAccount->id;
            $stripeCustomer->save();
        }

        return $stripeCustomer->stripe_customer_key;
    }

    private function getCustomerFullNameContext($customer)
    {
        $firstname = str_replace('"', '\\"', $customer->firstname);
        $lastname = str_replace('"', '\\"', $customer->lastname);

        return $firstname . ' ' . $lastname;
    }

    private function constructIntentData($cartId, $createCheckoutStatus, $captureMethod)
    {
        $captureMethod = ('on' == $captureMethod) ? 'manual' : 'automatic';

        $data = [
            'capture_method' => $captureMethod,
            'metadata' => [
                'id_cart' => $cartId,
            ],
            'description' => 'Product Purchase',
        ];

        if (false === $createCheckoutStatus) {
            if ('manual' === $captureMethod) {
                $data['capture_method'] = 'automatic';
                $data['payment_method_options'] = $this->getPaymentMethodOptions($captureMethod);
            }
            $data['automatic_payment_methods'] = ['enabled' => true];
        }

        return $data;
    }

    /**
     * @throws ApiErrorException
     * @throws \Exception
     */
    private function createIdempotencyKey($cart, $intentData, $sessionId = null)
    {
        $intent = [];
        $stripeIdempotencyKey = new \StripeIdempotencyKey();
        $stripeIdempotencyKey = $stripeIdempotencyKey->getByIdCart($cart->id);
        $this->logInfo('[ createIdempotencyKey ]' . json_encode($stripeIdempotencyKey), 'start');
        if ($sessionId) {
            $intent = $stripeIdempotencyKey->createForCheckout($cart->id, $sessionId);
            $this->logInfo('Create New Intent => ' . json_encode($intent), 'StripeTrait - createIdempotencyKey');
        } else {
            $paymentIntentStatus = null;
            $paymentIntentCaptureMethod = null;
            if (!empty($stripeIdempotencyKey->id)) {
                try {
                    $previousPaymentIntentData = PaymentIntent::retrieve($stripeIdempotencyKey->id_payment_intent);
                    $paymentIntentStatus = $previousPaymentIntentData->status;
                    $paymentIntentCaptureMethod = $previousPaymentIntentData->capture_method;
                } catch (ApiErrorException $e) {
                    $this->logInfo('Payment intent not found: ' . $stripeIdempotencyKey->id_payment_intent, 'StripeTrait - createIdempotencyKey');
                }
            }

            $updatableStatus = ['requires_payment_method', 'requires_confirmation', 'requires_action'];
            if (false === in_array($paymentIntentStatus, $updatableStatus)
                || $paymentIntentCaptureMethod !== $intentData['capture_method']
            ) {
                $this->logInfo('Create new stripeIdempotencyKey' . json_encode($intentData), 'StripeTrait - createIdempotencyKey');
                $intent = $stripeIdempotencyKey->createNewOne($cart->id, $intentData);
                $this->registerStripeEvent($intent, null, \StripeEvent::CREATED_STATUS);
            } else {
                $this->logInfo('Update stripeIdempotencyKey' . json_encode($intentData), 'StripeTrait - createIdempotencyKey');
                unset($intentData['capture_method']);
                $intent = $stripeIdempotencyKey->updateIntentData($intentData);
            }
        }

        return $intent;
    }

    /**
     * @throws \Exception
     */
    private function registerStripeEvent($intent, $eventCharge, $stripeEventStatus)
    {
        $stripeEventDate = new \DateTime();
        $dateCreated = (null != $eventCharge ? $eventCharge : $intent);
        $stripeEventDate = $stripeEventDate->setTimestamp($dateCreated->created);
        $this->logInfo('register Stripe Event=> ' . $stripeEventStatus . ' --- ' . json_encode($dateCreated), 'Stripe - registerStripeEvent');
        $stripeEvent = new \StripeEvent();
        $stripeEvent->setIdPaymentIntent($intent->id);
        $stripeEvent->setStatus($stripeEventStatus);
        $stripeEvent->setDateAdd($stripeEventDate->format('Y-m-d H:i:s'));
        $stripeEvent->setIsProcessed(true);
        $stripeEvent->setFlowType('direct');

        return $stripeEvent->save();
    }

    private function logInfo($message, $scope)
    {
        return [$message, $scope];
    }

    private function exceptionErrorLogger(string $message, $scope = '')
    {
        return [ 'error' => $message, 'scope' => $scope ];
    }
}
