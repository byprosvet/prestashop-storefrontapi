<?php

namespace Byprosvet\StorefrontApi\Checkout;

class StartRequest
{
    public Contact $contact;
    public Carrier $carrier;

    /** @var CartItem[] $cart **/
    public array $cart;

    public static function fromArray($payload)
    {
        $request = new self;
        $request->contact = Contact::fromArray($payload['contact']);
        $request->carrier = Carrier::fromArray($payload['carrier']);
        $request->cart = array_map(
            fn ($cartItem) => new CartItem($cartItem['id'], $cartItem['quantity']),
            $payload['cart']
        );
        return $request;
    }

    private function __construct()
    {
    }
}
