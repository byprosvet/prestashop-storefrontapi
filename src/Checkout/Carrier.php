<?php

namespace Byprosvet\StorefrontApi\Checkout;

class Carrier
{
    public string $countryCode;
    public string $id;
    public string $city;
    public string $address1;
    public ?string $address2;

    public static function fromArray($payload)
    {
        $carrier = new self;
        $carrier->id = $payload['id'];
        $carrier->countryCode = $payload['countryCode'];
        $carrier->city = $payload['city'] ?? 'Not set';
        $carrier->address1 = $payload['address1'] ?? 'Not set';
        $carrier->address2 = $payload['address2'];
        return $carrier;
    }
}

