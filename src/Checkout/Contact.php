<?php

namespace Byprosvet\StorefrontApi\Checkout;

class Contact
{
    public string $firstname;
    public string $lastname;
    public string $email;
    public string $phone;

    public static function fromArray($payload)
    {
        $contact = new self;
        $contact->firstname = empty($payload['firstname']) ? 'Unknown' : $payload['firstname'];
        $contact->lastname = empty($payload['lastname']) ? 'Unknown' : $payload['lastname'];
        $contact->email = $payload['email'];
        $contact->phone = $payload['phone'];
        return $contact;
    }
}
