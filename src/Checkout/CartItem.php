<?php

namespace Byprosvet\StorefrontApi\Checkout;

class CartItem
{
    public function __construct(
        public string $id,
        public int $quantity
    ) {}
}
