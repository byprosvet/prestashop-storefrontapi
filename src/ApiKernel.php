<?php

namespace Byprosvet\StorefrontApi;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiKernel
{
    public function __construct(
        private Router $router
    ) {}

    public function handle(Request $request)
    {
        if ($this->shouldIgnoreRequest($request)) {
            return;
        }
        $response = $this->runController($request);
        if ($response) {
            $response->send();
            $this->end();
        }
    }

    private function runController($request)
    {
        try {
            $matchAttributes = $this->router->match($request);
            if (!isset($matchAttributes)) {
                return;
            }
            $controller = $matchAttributes['_controller'];
            $action = $matchAttributes['action'];
            return call_user_func([$controller, $action], $request, $matchAttributes);
        } catch (\Throwable $error) {
            return new JsonResponse([
                'message' => $error->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function end()
    {
        die();
    }

    private function shouldIgnoreRequest(Request $request)
    {
        return strpos($request->getPathInfo(), '/modules') !== false;
    }
}
