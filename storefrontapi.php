<?php

use Symfony\Component\HttpFoundation\Request;
use PrestaShop\PrestaShop\Adapter\ContainerBuilder;
use Byprosvet\StorefrontApi\ApiKernel;

require_once __DIR__ . "/vendor/autoload.php";

if (!defined('_PS_VERSION_')) {
    exit;
}

class Storefrontapi extends Module
{
    public function __construct()
    {
        $this->name = 'storefrontapi';
        $this->tab = 'front_office_features';
        $this->version = '0.9.0';
        $this->author = 'ByProsvet';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.7.0.0',
            'max' => '8.99.99',
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('ByProsvet Storefront API');
        $this->description = $this->l('HTTP JSON API for Storefronts');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('actionDispatcherBefore');
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function getContent()
    {
        return "<h1>Storefront API Configuration</h1>";
    }

    public function hookActionDispatcherBefore($controller)
    {
        if ($controller['controller_type'] === 1) {
            $this->initializeStripe();
            $request = Request::createFromGlobals();
            $apiKernel = $this->getKernel();
            $apiKernel->handle($request);
        }
    }

    private function getKernel()
    {
        $container = ContainerBuilder::getContainer('front', _PS_MODE_DEV_);
        return $container->get(ApiKernel::class);
    }

    private function initializeStripe()
    {
        Module::getInstanceByName('stripe_official');
    }
}
